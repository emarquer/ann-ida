import random as rd
from copy import copy
from functools import partial
from statistics import mean

import click
import torch
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Subset

from analogy_clf import AnalogyClassification
from cnn_embeddings import CNNEmbedding
from config import (CLASSIFIER_EVAL_PATH, CLASSIFIER_PATH,
                    JAP_SPLIT_RANDOM_STATE)
from data import LANGUAGES, Task1Dataset, enrich, generate_negative
from utils import collate, elapsed_timer, get_accuracy_classification


@click.command()
@click.option('--language', '-l', default="arabic", help='The language to evaluate the model on.', type=click.Choice(LANGUAGES, case_sensitive=False))
@click.option('--epochs', '-e', default=20,
              help='The number of epochs the model was trained on (we use this parameter to use the right files).')
@click.option('--nb-analogies', '-n', default=50000,
              help='The maximum number of analogies (before augmentation) we evaluate the model on.'
              'If the number is greater than the number of analogies in the dataset, then all the analogies will be used.')
@click.option('--dropout', '-d', default=0,
              help='The dropout probability to use during evaluation. To disable dropout, use `0`.')
def evaluate_classifier(language, epochs, nb_analogies, dropout):
    '''Produces the accuracy for valid analogies, invalid analogies for a given model.

    Arguments:
    language -- The language of the model.
    epochs -- The number of epochs the models were trained on (we use this parameter to use the right files).
    nb_analogies -- The maximum number of analogies (before augmentation) we evaluate the model on. If the number is greater than the number of analogies in the dataset, then all the analogies will be used.
    dropout -- The dropout probability to use. To disable dropout, use `0`'''

    device = "cuda" if torch.cuda.is_available() else "cpu"

    train_dataset = Task1Dataset(language=language, mode="train", word_encoding="char")
    if language == "japanese":
        japanese_train_analogies, japanese_test_analogies = train_test_split(train_dataset.analogies, test_size=0.3, random_state = JAP_SPLIT_RANDOM_STATE)

        test_dataset = copy(train_dataset)
        test_dataset.analogies = japanese_test_analogies

        train_dataset.analogies = japanese_train_analogies
    else:
        test_dataset = Task1Dataset(language=language, mode="test", word_encoding="char")

    # Even the char:int dictionaries
    voc = train_dataset.word_voc_id
    BOS_ID = len(voc) # (max value + 1) is used for the beginning of sequence value
    EOS_ID = len(voc) + 1 # (max value + 2) is used for the end of sequence value

    test_dataset.word_voc = train_dataset.word_voc
    test_dataset.word_voc_id = voc

    #subsets
    if len(test_dataset) > nb_analogies:
        test_indices = list(range(len(test_dataset)))
        test_sub_indices = rd.sample(test_indices, nb_analogies)
        test_subset = Subset(test_dataset, test_sub_indices)
    else:
        test_subset = test_dataset
       
    #load data
    test_dataloader = DataLoader(test_subset, shuffle = True, collate_fn = partial(collate, bos_id = BOS_ID, eos_id = EOS_ID))

    saved_models = torch.load(CLASSIFIER_PATH.format(language=language, epochs=epochs))

    #load models
    if language == "japanese":
        emb_size = 512
    else:
        emb_size = 64

    embedding_model = CNNEmbedding(emb_size=emb_size, voc_size = len(voc) + 2)
    embedding_model.load_state_dict(saved_models['state_dict_embeddings'])
    embedding_model.eval()

    classification_model = AnalogyClassification(emb_size=16*5)
    classification_model.load_state_dict(saved_models['state_dict_classification'])
    classification_model.eval()

    #training loop
    embedding_model.to(device)
    classification_model.to(device)

    accuracy_true = []
    accuracy_false = []

    for a, b, c, d in test_dataloader:

        # compute the embeddings
        a = embedding_model(a.to(device))
        b = embedding_model(b.to(device))
        c = embedding_model(c.to(device))
        d = embedding_model(d.to(device))
        a = torch.unsqueeze(a, 1)
        b = torch.unsqueeze(b, 1)
        c = torch.unsqueeze(c, 1)
        d = torch.unsqueeze(d, 1)

        for a_, b_, c_, d_ in enrich(a, b, c, d):

            # positive example, target is 1
            is_analogy = torch.squeeze(classification_model(a_, b_, c_, d_, dropout))
            expected = torch.ones(is_analogy.size(), device=is_analogy.device)
            accuracy_true.append(get_accuracy_classification(expected, is_analogy))

            for a__, b__, c__, d__ in generate_negative(a_, b_, c_, d_):
                # negative examples, target is 0
                is_analogy = torch.squeeze(classification_model(a__, b__, c__, d__, dropout))
                expected = torch.zeros(is_analogy.size(), device=is_analogy.device)
                accuracy_false.append(get_accuracy_classification(expected, is_analogy))

    print(f'Accuracy for valid analogies: {mean(accuracy_true)}\nAccuracy for invalid analogies: {mean(accuracy_false)}')
    torch.save({
        "valid": mean(accuracy_true),
        "invalid": mean(accuracy_false)},
        CLASSIFIER_EVAL_PATH.format(language=language, epochs=epochs)) # f"evaluation/evaluation_classification_CNN_{language}_{epochs}e.pth"

if __name__ == '__main__':
    evaluate_classifier()
