# %%
import torch
import config
import data

for lang in data.LANGUAGES:
    d = torch.load(config.REGRESSION_EVAL_PATH.format(language=lang,epochs=20,duplicate_label=''))
    print(d[0][0].keys())
# %%
