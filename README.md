
# A*NN*aMorphose

Neural Network models for Analogy in Morphology (A*NN*aMorphose) is an approach using deep learning models for the identification and inference of analogies in morphology.
The approach presented here is described in the article "Tackling Morphological Analogies Using Deep Learning" (Anonymous, 2022).

To cite this repository, use the following reference:
```bib
@article{tackle-morpho-analogies:2022:annonymous,
    author = {To be added if the article is accepted},
    title = {Tackling Morphological Analogies Using Deep Learning},
}
```

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Install Instructions](#install-instructions)
  - [Cloning the Repository](#cloning-the-repository)
  - [Installing the Dependencies](#installing-the-dependencies)
  - [Installing Sigmorphon2016](#installing-sigmorphon2016)
    - [Setting-Up the Japanese Data](#setting-up-the-japanese-data)
  - [*[Optional]* Installing the EMbedding Models for the Preliminary Experiments](#optional-installing-the-embedding-models-for-the-preliminary-experiments)
    - [GloVe](#glove)
    - [Fasttext](#fasttext)
    - [w2v](#w2v)
- [General Usage](#general-usage)
- [Reproducing Experiments](#reproducing-experiments)
  - [Running the Baselines](#running-the-baselines)
    - [Installing Fam and Lepage (2018)](#installing-fam-and-lepage-2018)
  - [Classification Model](#classification-model)
    - [Training](#training)
    - [Evaluation](#evaluation)
    - [Evaluation with Dropout](#evaluation-with-dropout)
  - [Regression Model](#regression-model)
    - [Training](#training-1)
    - [Evaluation with Dropout](#evaluation-with-dropout-1)
    - [Testing Euclidean and Cossine Distance](#testing-euclidean-and-cossine-distance)
  - [Embedding with Various Filter Size and Number](#embedding-with-various-filter-size-and-number)
- [Files and Folders TO RE-CHECK](#files-and-folders-to-re-check)
  - [Snippets](#snippets)

## Install Instructions
The following installation instruction are designed for command line on Unix systems. Refer to the instructions for Git and Anaconda on your exploitation system for the corresponding instructions.

### Cloning the Repository
Clone the repository on your local machine, using the following command:

```bash
git clone --recurse-submodules git@gitlab.inria.fr:emarquer/ann-ida.git
```
This will download the repository and its *submodules* (external git repositories, in this case the SIGMORPHON16 dataset).

### Installing the Dependencies

Install Anaconda (or miniconda to save storage space).

Then, create a conda environement (for example `morpho-analogy`) and install the dependencies, using the following commands:

```bash
conda env create --name morpho-analogy python=3.9 -f environment.yml
```

All the following commands assume that you have activated the environment you just created. This can be done with the following command (using our example `morpho-analogy`):

```bash
conda activate morpho-analogy
```

### Installing Sigmorphon2016
To get the data of the Sigmorphon2016 dataset, run:
```bash
grep https://github.com/ryancotterell/sigmorphon2016/archive/refs/heads/master.zip sigmorphon2016.zip
unzip sigmorphon2016.zip -d sigmorphon2016
```

#### Setting-Up the Japanese Data
The Japanese data is stored as a Sigmorphon2016-style data file `japanese-task1-train` at the root of the directory, and should be moved to `sigmorphon2016/data`, the Sigmorphon2016 data folder. This can be done by running the following command:

```bash
mv japanese-task1-train sigmorphon2016/data/
```

There is no test and development set. For the training and evaluation, the file `japanese-task1-train` is split: 70\% of the analogies for the training and 30\% for the evaluation. The split is always the same for reproducibility, using random seed 42.

The Japanese data was extracted from the original [Japanese Bigger Analogy Test Set](https://vecto-data.s3-us-west-1.amazonaws.com/JBATS.zip).

### *[Optional]* Installing the Embedding Models for the Preliminary Experiments
#### GloVe
To get the GloVe embedding model for German and set it up so that the Dataset class from `data.py`, run:
```bash
mkdir embeddings/glove
grep https://int-emb-glove-de-wiki.s3.eu-central-1.amazonaws.com/vectors.txt embeddings/glove/vectors.txt
```
#### Fasttext
To get the *fasttext* embedding model for German and set it up so that the Dataset class from `embeddings/fasttext_.py`, run:
```bash
mkdir embeddings/fasttext
grep https://s3.eu-central-1.amazonaws.com/int-emb-fasttext-de-wiki/20180917/model.bin embeddings/fasttext/berman.bin
```

#### w2v
To get the *w2v* embedding model for German, run:
```bash
mkdir embeddings/w2v
grep https://int-emb-word2vec-de-wiki.s3.eu-central-1.amazonaws.com/vectors.txt embeddings/w2v/german-vectors.txt
```

To set it up so that the Dataset class from `embeddings/w2v.py` can interpret it, run:
```bash
conda activate morpho-analogy
python embeddings/w2v.py
```
The above command will launch the `convert` function of `embeddings/w2v.py` to transform the *w2v* vector file into a ready-to-use PyTorch pickle.

## General Usage
For each of the files, it is not necessary to fill the parameters when you run the code, default values are used. For each of the experiments file, you can use the `--help` flag to print the help message and detail available arguments.

## Reproducing Experiments
This section explains how to reproduce step by step the experiments reported in the article.

### Running the Baselines
To run the baselines, run `python run_baseline.py <language> <algorithm>` (ex: `python run_baseline.py arabic kolmo`).
This will output a summary in the command line interface as well as a CSV log file in the baseline folder (ex: `baseline/murena/arabic`).

The available languages are:
- `arabic`;
- `finnish`;
- `georgian`;
- `german`;
- `hungarian`;
- `maltese`;
- `navajo`;
- `russian`;
- `spanish`;
- `turkish`;
- `japanese` (if you followed the steps to setup the Japanese Bigger Analogy Test Set).

The available baselines are:
- `alea` for the approach of P. Langlais, F. Yvon, and P. Zweigenbaum *"Improvements in analogical learning: Application to translating multi-terms of the medical domain"*;
- `kolmo` or `kolmogorov` or `murena` for the approach of P.-A. Murena, M. Al-Ghossein, J.-L. Dessalles, and
A. Cornuéjols *"Solving analogies on words based on minimal complexity transformation"*;
- `lepage` for the annalogy classifier of the toolset presented in R. Fam and Y. Lepage *"Tools for the production of analogical grids and a resource of n-gram analogical grids in 11 languages"*; the tools must be installed beforehand as described below.

#### Installing Fam and Lepage (2018)
First, check that you have `swig` installed (`sudo apt install swig` on Ubuntu-based systems).

Then, run the following from the home directory to install Lepage's algorithm:
```bash
cd baseline/lepage
conda activate morpho-analogy
pip install -e .
```

### Classification Model

#### Training
To **train** a classifier and the corresponding embedding model for a language, run the following (all parameters are optional, shorthands can be seen with the `--help` flag of the command):

```
python train_clf.py --language=<language> --nb-analogies=<number of analogies to use> --epochs=<number of epochs>
```
Examples: 
- `python train_clf.py -l arabic` to train on Arabic, using up to 50000 analogies (default) for 20 epochs (default);
- `python train_clf.py --language=german --nb-analogies=50000 --epochs=20` to train on German, using up to 50000 analogies for 20 epochs.


The output files follow the template  `models/classification_8x8/<language>_<epochs>e_vectors.txt`.

#### Evaluation
To **evaluate** a classifier, run the following (all parameters are optional, shorthands can be seen with the `--help` flag of the command):
```
python eval_clf.py --language=<language of the classifier> --epochs=<number of epochs the model was trained for> --nb-analogies=<number of analogies to use>
```
Examples: 
- `python eval_clf.py -l german -e 20` to evaluate the model trained on German for 20 epochs, using up to 50000 analogies (default);
- `python eval_clf.py --language=german --nb-analogies=500` to evaluate the model trained on German for 20 epochs (default), using up to 500 analogies.

The input files are `models/classification_8x8/<language>_<epochs>e_vectors.txt` produced by the training script.
The output files follow the template `evaluation/classification_8x8/<language>_<epochs>e.pth`.

#### Evaluation with Dropout
To **evaluate** a classifier **with dropout**, run the evaluation command with the `--dropout` parameter (`-d`).

Examples: 
- `python eval_clf.py -l german -e 20 -d 0.5` to evaluate the model trained on German for 20 epochs, using up to 50000 analogies (default), using an embedding dropout probability of $p_d=0.5$;
- `python eval_clf.py --dropout=0.01 --language=german --nb-analogies=500` to evaluate the model trained on German for 20 epochs (default), using up to 500 analogies, using an embedding dropout probability of $p_d=0.01$.

### Regression Model
#### Training
To **train** a regression model and the corresponding embedding model for a language, run the following (all parameters are optional, shorthands can be seen with the `--help` flag of the command):

```
python train_reg.py \
    --language=<language> \
    --nb-analogies=<number of analogies to use> \
    --epochs=<number of epochs> \
    --epochs-freeze=<number of epochs for which the embedding will be frozen> \
    --repetitions=<number of repetitions of the dropout experiment (only if --test is used)> \
    --duplicate-id=<id to use to create multiple versions of the regression model> \
    (--test)
```
Examples: 
- `python train_reg.py -l arabic` to train on Arabic, using up to 50000 analogies (default) for 20 epochs (default) while freezing for 10 epochs (default);
- `python train_reg.py --language=german --nb-analogies=50000 --epochs=20 -f 3` to train on German, using up to 50000 analogies for 20 epochs while freezing for 3 epochs.

The input files are `models/classification_8x8/<language>_<epochs>e.txt` produced by the classifier training script.
The output files follow the templates:
- `models/regression/<language>_<epochs>e.pth` (or `models/regression/<language>_<epochs>e_v<dulicate-id>.pth`) for the model;
- `models/regression/<language>_<epochs>e_vectors.txt` (or `models/regression/<language>_<epochs>e_v<dulicate-id>_vectors.txt`) for a pre-computed embedding of all the words in the training data, to be used for evaluation.

#### Evaluation with Dropout
To **evaluate** a regression model and the corresponding embedding model for a language, without dropout and with dropout ranging from $0.005$ to $0.5$, run the following (all parameters are optional, shorthands can be seen with the `--help` flag of the command):

```
python eval_reg.py \
    --language=<language> \
    --nb-analogies=<number of analogies to use> \
    --epochs=<number of epochs during training> \
    --repetitions=<number of repetitions of the dropout experiment (only if --test is used)> \
    --duplicate-id=<id to use to create multiple versions of the regression model>
```
Examples: 
- `python eval_reg.py -l german -e 20` to evaluate the model trained on German for 20 epochs, using up to 50000 analogies (default), repeating the dropout experiments 10 times (default);
- `python eval_reg.py --language=german --nb-analogies=500 -r 5` to evaluate the model trained on German for 20 epochs (default), using up to 500 analogies, repeating the dropout experiments 5 times.

The input files are `models/regression/<language>_<epochs>e_vectors.txt` (or `models/regression/<language>_<epochs>e_v<dulicate-id>_vectors.txt`) produced by the training script.
The output files follow the template `evaluation/regression/<language>_<epochs>e_scores.pth` (or `evaluation/regression/<language>_<epochs>e_v<dulicate-id>_scores.pth`).

#### Testing Euclidean and Cossine Distance
To **evaluate** all the regression models and the corresponding embedding models for Euclidean and cossine distance, with various acceptance thresholds, run the following (all parameters are optional, shorthands can be seen with the `--help` flag of the command):

```
python eval_reg_extended.py \
    --search-range=<maximum percentage on the distance to the predicted vector> \
    --language=<language> \
    --nb-analogies=<number of analogies to use> \
    --epochs=<number of epochs during training> \
    --duplicate-id=<id to use to create multiple versions of the regression model> \
    (--more_statistical_tests)
```

Example:
- `python eval_reg_extended.py --nb-analogies=50000 --search-range=5`.
 
You can add the flag `--more_statistical_tests` so that more tests will be run during the execution of the code.

The input files are `models/regression/<language>_<epochs>e_vectors.txt` (or `models/regression/<language>_<epochs>e_v<dulicate-id>_vectors.txt`) produced by the training script.
The output files follow the template `evaluation/regression/euclid_cosine_r<search-range>_<epochs>e_scores.pth` (or `evaluation/regression/euclid_cosine_r<search-range>_<epochs>e_v<dulicate-id>_scores.pth`).

### Embedding with Various Filter Size and Number
To **train and evaluate** a classifier and the corresponding embedding model for a language, run the following (all parameters are optional, shorthands can be seen with the `--help` flag of the command):

```
python variable_sizes/train_clf_var_sizes.py \
    --language=<language> \
    --nb-analogies=<number of analogies to use> \
    --epochs=<number of epochs during training> \
    --max-filter-size=<maximum size of the filters> \
    --n-filters=<number of filters per size>
```

Example:
- `python variable_sizes/train_clf_var_sizes.py --nb-analogies=1000 --n-filters=32 --max-filter-size=3` to train and evaluate a classifier with an embedding model whith 32 filters of size up to 3, *i.e.*, 32 filters spaning 2 characters and 32 filters spaning 3 characters; this command performs the training on Arabic (default) for 20 epochs (default) using 1000 samples.

The input files are `models/regression/<language>_<epochs>e_vectors.txt` (or `models/regression/<language>_<epochs>e_v<dulicate-id>_vectors.txt`) produced by the training script.
The output files follow the template `evaluation/regression/euclid_cosine_r<search-range>_<epochs>e_scores.pth` (or `evaluation/regression/euclid_cosine_r<search-range>_<epochs>e_v<dulicate-id>_scores.pth`).

## Files and Folders TO RE-CHECK
Folders in the directory:
- `baseline`: scripts to run each of the 3 baselines (`alea`, `kolmo`, and `lepage`);
- `embeddings`: scripts (and, once donloaded, model files) of the pre-trained embeddings used in our experiments;
- `models`: model files generated by the training scripts;
- `evaluation`: output files of our evaluation scripts;
- `sigmorphon2016`: data files of the Sigmorphon2016 dataset;
- `snippets`: several scripts used throught the devlopment of the approach, typically for evaluation and plotting;
- `variable_sizes`: scripts to experiment with embedding models with different number and size of filters.

Files at the root of the directory:
- `analogy_clf.py`: analogy classifier model definition;
- `analogy_reg.py`: analogy regression model definition;
- `cnn_embeddings.py`: morphological embedding model definition;
- `config.py`: file centralizing the most important paths and path templates of the project;
- `data.py`: tools to load Sigmorphon2016 datasets, contains the main dataset class `Task1Dataset` and the data augmentation functions `enrich`, `generate_negative`, and `generate_negative`; a dataset class `Task2Dataset` is also provided for potential experiments using the data from task 2 of Sigmorphon2016;
- `environment.yml`: file containing the environment data to create the anaconda environment;
- `eval_clf.py`: file to evaluate a classifier and the corresponding embedding model on the language it was trained on;
- `eval_reg.py`: file to evaluate a regression model and the corresponding embedding model on the language it was trained on;
- `eval_reg_extended.py`: file to evaluate a regression model and the corresponding embedding model on the language it was trained on, with some options to explore the Euclidean and cosine distance performance;
- `japanese-task1-train`: backup of the japanese data extracted from the Japanese Bigger Analogy Test Set, in the Sigmorphon2016 task 1 format; to be used if Sigmorphon2016 has to be downloaded manually;
- `README.md`: this file;
- `run_baselines.py`: file to run one of the 3 baselines (`alea`, `kolmo`, and `lepage`) on a given language;
- `train_clf_fasttext.py`: file to train a classifier model and pretrained *fasttext* embedding model on German;
- `train_clf_w2v.py`: file to train a classifier model and pretrained *w2v* embedding model on German;
- `train_clf.py`: file to train a classifier model and the corresponding embedding model on a given language;
- `train_multilingual_clf_multiemb.py`: file to train a multilingual classifier model and an embedding model per language, on all languages (Japanese optionnal);
- `train_multilingual_clf.py`: file to train a multilingual classifier model and the corresponding embedding model on all languages (Japanese optionnal);
- `train_clf.py`: file to train a regression model and the corresponding embedding model on a given language;
- `utils.py`: generic tools used throught the code are stored in this file.

### Snippets
The `snippets` folder contains files with frequently used code snippets:
- loading data based on a language and a number of epochs;
- ploting diagrams & other stats.

Run `python snippets/ttest/ttest.py --path=<path to your file>` to compare the columns of a CSV file (ex: `python snippets/ttest/ttest.py --path=valid.csv`).
