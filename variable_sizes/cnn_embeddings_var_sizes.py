from statistics import mean

import torch
import torch.nn as nn


class CNNEmbedding(nn.Module):
    def __init__(self, emb_size, voc_size, max_filter_size=6, n_filters=16):
        ''' Character level CNN word embedding.
        It produces an output of length 80 by applying filters of different sizes on the input.
        It uses n_filters filters for each size from 2 to max_filter_size.
        Arguments:
        emb_size -- the size of the input vectors
        voc_size -- the maximum number to find in the input vectors
        max_filter_size -- the maximum size of the CNN filter along the caracter dimension
        n_filters -- the number of filters per size
        '''
        super().__init__()

        self.emb_size = emb_size
        self.voc_size = voc_size

        self.embedding = nn.Embedding(voc_size, emb_size)

        self.convs = nn.ModuleList([
            nn.Conv1d(emb_size, n_filters, i, padding = i//2)
        for i in range(2, max_filter_size+1)])


    def forward(self, word):
        # Embedds the word and set the right dimension for the tensor
        unk = word<0
        word[unk] = 0
        word = self.embedding(word)
        word[unk] = 0
        word = torch.transpose(word, 1,2)

        # Apply each conv layer -> torch.Size([batch_size, 16, whatever])
        conv_outputs = [conv(word) for conv in self.convs]

        # Get the max of each channel -> torch.Size([batch_size, 16])
        maxima = [torch.max(conv_output, dim=-1)[0] for conv_output in conv_outputs]

        # Concatenate the 5 vectors to get 1 -> torch.Size([batch_size, 80])
        output = torch.cat(maxima, dim = -1)

        return output
