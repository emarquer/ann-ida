import random as rd
from copy import copy
from functools import partial
from statistics import mean

import click
import torch
import torch.nn as nn
from analogy_clf import AnalogyClassification
from config import CLASSIFIER_VAR_SIZE_PATH
from data import (Task1Dataset, enrich, generate_negative,
                  random_sample_negative)
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Subset
from utils import collate, elapsed_timer, get_accuracy_classification

from variable_sizes.cnn_embeddings_var_sizes import CNNEmbedding


@click.command()
@click.option('--nb-analogies', default=50000,
              help='The maximum number of analogies (before augmentation) we train the model on. If the number is greater than the number of analogies in the dataset, then all the analogies will be used.', show_default=True)
@click.option('--epochs', default=20,
              help='The number of epochs we train the model for.', show_default=True)
@click.option('--language', default="arabic", help='The language to train the model on.', show_default=True)
@click.option('--max-filter-size', default=6, help='The maximum size of the filters.', show_default=True)
@click.option('--n-filters', default=16, help='The number of filters per size.', show_default=True)
def train_classifier(language, nb_analogies, epochs, max_filter_size, n_filters):
    '''Trains a classifier and a word embedding model for a given language.

    Arguments:
    nb_analogies -- The number of analogies to use (before augmentation) for the training.
    epochs -- The number of epochs we train the model for.
    language -- The language of the data to use for the training.
    max_filter_size -- The maximum size of the filters, the smallest filters are of size 2.
    n_filters -- The number of filters per size.'''

    print(f"{language}: {nb_analogies} analogies for {epochs} epochs with {n_filters} filters for sizes up to {max_filter_size}")

    device = "cuda" if torch.cuda.is_available() else "cpu"

    def get_accuracy(y_true, y_prob):
        assert y_true.size() == y_prob.size()
        y_prob = y_prob > 0.5
        if y_prob.ndim > 1:
            return (y_true == y_prob).sum().item() / y_true.size(0)
        else:
            return (y_true == y_prob).sum().item()


    # --- Prepare data ---

    ## Train and test dataset
    train_dataset = Task1Dataset(language=language, mode="train", word_encoding="char")
    if language == "japanese":
        japanese_train_analogies, japanese_test_analogies = train_test_split(train_dataset.analogies, test_size=0.3, random_state = 42)

        test_dataset = copy(train_dataset)
        test_dataset.analogies = japanese_test_analogies

        train_dataset.analogies = japanese_train_analogies
    else:
        test_dataset = Task1Dataset(language=language, mode="test", word_encoding="char")

    voc = train_dataset.word_voc_id
    BOS_ID = len(voc) # (max value + 1) is used for the beginning of sequence value
    EOS_ID = len(voc) + 1 # (max value + 2) is used for the end of sequence value

    # Get subsets

    if len(train_dataset) > nb_analogies:
        train_indices = list(range(len(train_dataset)))
        train_sub_indices = rd.sample(train_indices, nb_analogies)
        train_subset = Subset(train_dataset, train_sub_indices)
    else:
        train_subset = train_dataset

    if len(test_dataset) > nb_analogies:
        test_indices = list(range(len(test_dataset)))
        test_sub_indices = rd.sample(test_indices, nb_analogies)
        test_subset = Subset(test_dataset, test_sub_indices)
    else:
        test_subset = test_dataset


    # Load data
    train_dataloader = DataLoader(train_subset, shuffle = True, collate_fn = partial(collate, bos_id = BOS_ID, eos_id = EOS_ID))
    test_dataloader = DataLoader(test_subset, shuffle = True, collate_fn = partial(collate, bos_id = BOS_ID, eos_id = EOS_ID))

    # --- Training models ---
    if language == 'japanese':
        emb_size = 512
    else:
        emb_size = 64

    classification_model = AnalogyClassification(emb_size=n_filters*(max_filter_size-1))
    embedding_model = CNNEmbedding(emb_size=emb_size, voc_size = len(voc) + 2, max_filter_size=max_filter_size, n_filters=n_filters)

    # --- Training Loop ---


    classification_model.to(device)
    embedding_model.to(device)

    optimizer = torch.optim.Adam(list(classification_model.parameters()) + list(embedding_model.parameters()))
    criterion = nn.BCELoss()

    losses_list = []
    times_list = []

    for epoch in range(epochs):

        losses = []
        with elapsed_timer() as elapsed:
            for a, b, c, d in train_dataloader:

                optimizer.zero_grad()

                # compute the embeddings
                a = embedding_model(a.to(device))
                b = embedding_model(b.to(device))
                c = embedding_model(c.to(device))
                d = embedding_model(d.to(device))
                a = torch.unsqueeze(a, 1)
                b = torch.unsqueeze(b, 1)
                c = torch.unsqueeze(c, 1)
                d = torch.unsqueeze(d, 1)

                loss = torch.tensor(0).to(device).float()

                for a_, b_, c_, d_ in enrich(a, b, c, d):
                    # positive example, target is 1
                    is_analogy = classification_model(a_, b_, c_, d_)

                    expected = torch.ones(is_analogy.size(), device=is_analogy.device)
                    loss += criterion(is_analogy, expected)

                for a_, b_, c_, d_ in random_sample_negative(a, b, c, d):
                    # negative example, target is 0
                    is_analogy = classification_model(a_, b_, c_, d_)

                    expected = torch.zeros(is_analogy.size(), device=is_analogy.device)
                    loss += criterion(is_analogy, expected)


                # once we have all the losses for one set of embeddings, we can backpropagate
                loss.backward()
                optimizer.step()

                losses.append(loss.cpu().item())

        losses_list.append(mean(losses))
        times_list.append(elapsed())
        print(f"Epoch: {epoch}, Run time: {times_list[-1]:4.5}s, Loss: {losses_list[-1]}")

    torch.save({"state_dict_classification": classification_model.cpu().state_dict(), "state_dict_embeddings": embedding_model.cpu().state_dict(), "losses": losses_list, "times": times_list, "word_voc_id": train_dataset.word_voc_id, "word_voc": train_dataset.word_voc}, f"classification/variables_sizes/classification_CNN_{language}_{epochs}e_{max_filter_size}s_{n_filters}f.pth")

    ######## Test models ########

    accuracy_true = []
    accuracy_false = []

    with elapsed_timer() as elapsed:

        #i = 0

        for a, b, c, d in test_dataloader:

            # compute the embeddings
            a = embedding_model(a.to(device))
            b = embedding_model(b.to(device))
            c = embedding_model(c.to(device))
            d = embedding_model(d.to(device))
            a = torch.unsqueeze(a, 1)
            b = torch.unsqueeze(b, 1)
            c = torch.unsqueeze(c, 1)
            d = torch.unsqueeze(d, 1)

            for a_, b_, c_, d_ in enrich(a, b, c, d):

                # positive example, target is 1
                is_analogy = torch.squeeze(classification_model(a_, b_, c_, d_))
                expected = torch.ones(is_analogy.size(), device=is_analogy.device)
                accuracy_true.append(get_accuracy_classification(expected, is_analogy))

                for a__, b__, c__, d__ in generate_negative(a_, b_, c_, d_):
                    # negative examples, target is 0
                    is_analogy = torch.squeeze(classification_model(a__, b__, c__, d__))
                    expected = torch.zeros(is_analogy.size(), device=is_analogy.device)
                    accuracy_false.append(get_accuracy_classification(expected, is_analogy))


        print(f'Run time: {elapsed():4.5}s\nAccuracy for valid analogies: {mean(accuracy_true)}\nAccuracy for invalid analogies: {mean(accuracy_false)}')

    torch.save({"state_dict_classification": classification_model.cpu().state_dict(), "state_dict_embeddings": embedding_model.cpu().state_dict(), "losses": losses_list, "times": times_list, "word_voc_id": train_dataset.word_voc_id, "word_voc": train_dataset.word_voc, "accuracies": [mean(accuracy_true), mean(accuracy_false)]},
        CLASSIFIER_VAR_SIZE_PATH.format(language=language, epochs=epochs, max_filter_size=max_filter_size, n_filters=n_filters))


if __name__ == '__main__':
    train_classifier()
