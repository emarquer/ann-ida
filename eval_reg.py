#!/usr/bin/env python
# coding: utf-8

import random as rd
from functools import partial
from statistics import mean, stdev

import click
import torch
import torchtext.vocab as vocab
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Subset

from analogy_reg import AnalogyRegression
from cnn_embeddings import CNNEmbedding
from config import (REGRESSION_CACHE, REGRESSION_EVAL_PATH, REGRESSION_PATH,
                    REGRESSION_VECTORS_PATH, duplicate_label)
from data import LANGUAGES, Task1Dataset, enrich
from utils import collate, elapsed_timer

device = "cuda" if torch.cuda.is_available() else "cpu"

DROPOUTS = [0,0.005,0.01,0.05,0.1,0.3,0.5]

@click.command()
@click.option('--language', '-l', default="german", help='The language to evaluate the model on.', show_default=True, type=click.Choice(LANGUAGES, case_sensitive=False))
@click.option('--nb-analogies', '-n', default=50000,
              help='The maximum number of analogies (before augmentation) we evaluate the model on.' 
              'If the number is greater than the number of analogies in the dataset, then all the analogies will be used.', show_default=True)
@click.option('--epochs','-e', default=20,
              help='The number of epochs we trained the model for.', show_default=True)
@click.option('--repetitions','-r', default=10,
              help='The number of repetitions of the evaluation.', show_default=True)
@click.option('--duplicate-id','-d', default=0,
              help='The id to use to identify the model in case of an intended duplicate with the same language and number of epochs.', show_default=True)
def eval_reg(language, nb_analogies, epochs, repetitions, duplicate_id):
    eval_reg_(language, nb_analogies, epochs, repetitions, duplicate_id)

def eval_reg_(language, nb_analogies, epochs, repetitions, duplicate_id):
    path = REGRESSION_PATH.format(language=language, epochs=epochs, duplicate_label=duplicate_label(duplicate_id))
    result_path = REGRESSION_EVAL_PATH.format(language=language, epochs=epochs, duplicate_label=duplicate_label(duplicate_id))
    custom_embeddings = vocab.Vectors(name = REGRESSION_VECTORS_PATH.format(language=language, epochs=epochs, duplicate_label=duplicate_label(duplicate_id)),
                                    cache = REGRESSION_CACHE,
                                    unk_init = torch.Tensor.normal_)

    custom_embeddings.vectors = custom_embeddings.vectors.to(device)

    # Cosine distance
    stored_lengths = torch.sqrt((custom_embeddings.vectors ** 2).sum(dim=1))

    def closest_cosine(vec):
        numerator = (custom_embeddings.vectors * vec).sum(dim=1)
        denominator = stored_lengths * torch.sqrt((vec ** 2).sum())
        similarities = numerator / denominator
        return custom_embeddings.itos[similarities.argmax()]

    # Euclidian distance
    def closest_euclid(vec):
        dists = torch.sqrt(((custom_embeddings.vectors - vec) ** 2).sum(dim=1))
        return custom_embeddings.itos[dists.argmin()]


    ######## Test models ########
    data = torch.load(path)

    # --- Data ---
    if language == "japanese":
        test_dataset = Task1Dataset(language=language, mode="train", word_encoding="char")
        japanese_train_analogies, japanese_test_analogies = train_test_split(test_dataset.analogies, test_size=0.3, random_state = 42)
        test_dataset.analogies = japanese_test_analogies
    else:
        test_dataset = Task1Dataset(language=language, mode="test", word_encoding="char")
    # Same dict
    test_dataset.word_voc = data['word_voc']
    test_dataset.word_voc_id = data['word_voc_id']
    BOS_ID = len(data['word_voc']) # (max value + 1) is used for the beginning of sequence value
    EOS_ID = len(data['word_voc']) + 1
    if len(test_dataset) > nb_analogies:
        test_indices = list(range(len(test_dataset)))
        test_sub_indices = rd.sample(test_indices, nb_analogies)
        test_subset = Subset(test_dataset, test_sub_indices)
    else:
        test_subset = test_dataset
    test_dataloader = DataLoader(test_subset, collate_fn = partial(collate, bos_id = BOS_ID, eos_id = EOS_ID), num_workers=4)

    # --- Model ---
    if language == "japanese":
        emb_size = 512
    else:
        emb_size = 64

    regression_model = AnalogyRegression(emb_size=16*5) # 16 because 16 filters of each size, 5 because 5 sizes
    regression_model.load_state_dict(data['state_dict'])
    regression_model.eval()
    embedding_model = CNNEmbedding(emb_size=emb_size, voc_size = EOS_ID + 1)
    embedding_model.load_state_dict(data['state_dict_embeddings'])
    embedding_model.eval()

    regression_model.to(device)
    embedding_model.to(device)

    results = dict()
    with torch.no_grad():
        with elapsed_timer() as total_elapsed:
            for dropout in DROPOUTS:
                result = []
                for i in range(repetitions if dropout>0 else 1):
                    accuracy_cosine = []
                    accuracy_euclid = []
                    with elapsed_timer() as elapsed:
                        for a, b, c, d in test_dataloader:

                            # compute the embeddings
                            a = embedding_model(a.to(device))
                            b = embedding_model(b.to(device))
                            c = embedding_model(c.to(device))
                            d = embedding_model(d.to(device))

                            for a_, b_, c_, d_expected in enrich(a, b, c, d):

                                d_pred = regression_model(a_, b_, c_, p=dropout)
                                d_closest_cosine = closest_cosine(d_pred)
                                d_closest_euclid = closest_euclid(d_pred)

                                d_expected_closest_cosine = closest_cosine(d_expected)
                                d_expected_closest_euclid = closest_euclid(d_expected)
                                
                                accuracy_cosine.append(1 if d_expected_closest_cosine == d_closest_cosine else 0)
                                accuracy_euclid.append(1 if d_expected_closest_euclid == d_closest_euclid else 0)

                    print(f'Dropout {dropout}, Repetition {i+1}/{repetitions if dropout>0 else 1}, Run time: {elapsed():4.5}s, Accuracy cosine: {mean(accuracy_cosine)}, Accuracy euclid: {mean(accuracy_euclid)}')

                    # result.append({
                    #     "cosine accuracy": {"mean": mean(accuracy_cosine), "std": stdev(accuracy_cosine)},
                    #     "euclid accuracy": {"mean": mean(accuracy_euclid), "std": stdev(accuracy_euclid)}
                    # })
                    result.append({"mean": mean(accuracy_euclid), "std": stdev(accuracy_euclid)})
                results[dropout] = result

                torch.save(results, result_path) # frequent saves for safety
        total = total_elapsed()
        print(f"Total training time: {total:4.5}s = {total/60:4.5}min = {total/3600:4.5}h")
        for dropout in DROPOUTS:
            means = [x["mean"] for x in results[dropout]]
            print(f"Dropout: {dropout}, euclidian accuracy average: {mean(means):.5f}{f' ± {stdev(means):.5f}' if len(means)>1 else ''}")

if __name__ == '__main__':
    eval_reg()
