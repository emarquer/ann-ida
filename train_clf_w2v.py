import random as rd
from copy import copy
from functools import partial
from statistics import mean

import click
import torch
import torch.nn as nn
import torch.nn.functional as F
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Subset

from analogy_clf import AnalogyClassification
from data import (LANGUAGES, Task1Dataset, enrich, generate_negative,
                  random_sample_negative)
from embeddings.w2v import W2V
from utils import elapsed_timer, get_accuracy_classification


@click.command()
@click.option('--language', '-l', default="arabic", help='The language to train the model on.', type=click.Choice(LANGUAGES, case_sensitive=False))
@click.option('--nb-analogies', '-n', default=50000,
              help='The maximum number of analogies (before augmentation) we train the model on.'
              'If the number is greater than the number of analogies in the dataset, then all the analogies will be used.')
@click.option('--epochs', '-e', default=20,
              help='The number of epochs we train the model for.')
def train_classifier(language, nb_analogies, epochs):
    '''Trains a classifier and a word embedding model for a given language.

    Arguments:
    language -- The language of the data to use for the training.
    nb_analogies -- The number of analogies to use (before augmentation) for the training.
    epochs -- The number of epochs we train the model for.'''
    device = "cuda" if torch.cuda.is_available() else "cpu"

    def get_accuracy(y_true, y_prob):
        assert y_true.size() == y_prob.size()
        y_prob = y_prob > 0.5
        if y_prob.ndim > 1:
            return (y_true == y_prob).sum().item() / y_true.size(0)
        else:
            return (y_true == y_prob).sum().item()


    # --- Prepare data ---

    ## Train and test dataset
    train_dataset = Task1Dataset(language=language, mode="train", word_encoding="none")

    if language == "japanese":
        japanese_train_analogies, japanese_test_analogies = train_test_split(train_dataset.analogies, test_size=0.3, random_state = 42)

        japanese_test_dataset = copy(train_dataset)
        japanese_test_dataset.analogies = japanese_test_analogies

        train_dataset.analogies = japanese_train_analogies
    else:
        test_dataset = Task1Dataset(language=language, mode="test", word_encoding="none")

    if len(test_dataset) > nb_analogies:
        test_indices = list(range(len(test_dataset)))
        test_sub_indices = rd.sample(test_indices, nb_analogies)
        test_subset = Subset(test_dataset, test_sub_indices)
    else:
        test_subset = test_dataset

    # Get subsets
    if len(train_dataset) > nb_analogies:
        train_indices = list(range(len(train_dataset)))
        train_sub_indices = rd.sample(train_indices, nb_analogies)
        train_subset = Subset(train_dataset, train_sub_indices)
    else:
        train_subset = train_dataset


    # Load data
    embedding_model = W2V(language)
    def collate(batch):
        a_emb, b_emb, c_emb, d_emb = [], [], [], []

        for a, b, c, d in batch:
            a_emb.append(embedding_model[a])
            b_emb.append(embedding_model[b])
            c_emb.append(embedding_model[c])
            d_emb.append(embedding_model[d])

        # make a tensor of all As, af all Bs, of all Cs and of all Ds
        a_emb = torch.stack(a_emb)
        b_emb = torch.stack(b_emb)
        c_emb = torch.stack(c_emb)
        d_emb = torch.stack(d_emb)

        return a_emb, b_emb, c_emb, d_emb
            
    train_dataloader = DataLoader(train_subset, shuffle = True, collate_fn = collate, num_workers=4)
    test_dataloader = DataLoader(test_subset, shuffle = True, collate_fn = collate, num_workers=4)

    # --- Training models ---
    classification_model = AnalogyClassification(emb_size=300)

    # --- Training Loop ---
    classification_model.to(device)

    optimizer = torch.optim.Adam(list(classification_model.parameters()))
    criterion = nn.BCELoss()

    losses_list = []
    times_list = []

    for epoch in range(epochs):

        losses = []
        with elapsed_timer() as elapsed:
            for a, b, c, d in train_dataloader:

                optimizer.zero_grad()

                # to be able to add other losses, which are tensors, we initialize the loss as a 0 tensor
                loss = torch.tensor(0).to(device).float()
                a = torch.unsqueeze(a, 0)
                b = torch.unsqueeze(b, 0)
                c = torch.unsqueeze(c, 0)
                d = torch.unsqueeze(d, 0)

                data = [[a, b, c, d]]

                for a, b, c, d in enrich(data):

                    # positive example, target is 1

                    is_analogy = classification_model(a, b, c, d)

                    expected = torch.ones(is_analogy.size(), device=is_analogy.device)

                    loss += criterion(is_analogy, expected)

                for a, b, c, d in random_sample_negative(data):

                    # negative examples, target is 0

                    is_analogy = classification_model(a, b, c, d)

                    expected = torch.zeros(is_analogy.size(), device=is_analogy.device)

                    loss += criterion(is_analogy, expected)

                # once we have all the losses for one set of embeddings, we can backpropagate
                loss.backward()
                optimizer.step()

                losses.append(loss.cpu().item())

        losses_list.append(mean(losses))
        times_list.append(elapsed())
        print(f"Epoch: {epoch}, Run time: {times_list[-1]:4.5}s, Loss: {losses_list[-1]}")

    torch.save({"state_dict_classification": classification_model.cpu().state_dict(), "losses": losses_list, "times": times_list}, f"models/classification_w2v_{language}_{epochs}e.pth")




    classification_model.eval()
    classification_model.to(device)

    accuracy_true = []
    accuracy_false = []

    for a, b, c, d in test_dataloader:
        a = torch.unsqueeze(a, 0)
        b = torch.unsqueeze(b, 0)
        c = torch.unsqueeze(c, 0)
        d = torch.unsqueeze(d, 0)

        data = [[a, b, c, d]]

        for a, b, c, d in enrich(data):

            # positive example, target is 1

            is_analogy = torch.squeeze(classification_model(a, b, c, d))

            expected = torch.ones(is_analogy.size(), device=is_analogy.device)

            accuracy_true.append(get_accuracy_classification(expected, is_analogy))

            for a, b, c, d in generate_negative([[a, b, c, d]]):

                # negative examples, target is 0

                is_analogy = torch.squeeze(classification_model(a, b, c, d))

                expected = torch.zeros(is_analogy.size(), device=is_analogy.device)

                accuracy_false.append(get_accuracy_classification(expected, is_analogy))

    print(f'Accuracy for valid analogies: {mean(accuracy_true)}\nAccuracy for invalid analogies: {mean(accuracy_false)}')



if __name__ == '__main__':
    train_classifier()
