import random as rd
from functools import partial
from statistics import mean

import click
import torch
import torch.nn as nn
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Subset

from analogy_clf import AnalogyClassification
from cnn_embeddings import CNNEmbedding
from config import (CLASSIFIER_MULTILINGUAL_PATH, JAP_SPLIT_RANDOM_STATE,
                    classifier_multilingual_mode)
from data import LANGUAGES, Task1Dataset, enrich, generate_negative, random_sample_negative
from utils import collate, elapsed_timer


@click.command()
@click.option('--nb-analogies', '-n', default=50000,
              help='The maximum number of analogies (before augmentation) we train the model on. If the number is greater than the number of analogies in the dataset, then all the analogies will be used.', show_default=True)
@click.option('--epochs', '-e', default=20,
              help='The number of epochs we train the model for.', show_default=True)
@click.option('--exclude-jap/--use-jap', '-nj/-j', default=True)
def train_classifier(nb_analogies, epochs, exclude_jap, original_balance):
    '''Trains a classifier and a word embedding model for a given language.

    Arguments:
    nb_analogies -- The number of analogies to use (before augmentation) for the training.
    epochs -- The number of epochs we train the model for.'''

    device = "cuda:0" if torch.cuda.is_available() else "cpu"

    # Data paralelism must be done before putting device on CUDA
    data_parallel = False
    if torch.cuda.device_count() > 1:
        print(f"Using DataParallel on {torch.cuda.device_count()} GPUs")
        data_parallel = True
    elif torch.cuda.device_count()==1:
        print(f"Using 1 GPU (no DataParallel)")
        
    # --- Prepare data ---

    ## Load train data and unify vocabulary
    train_datasets = dict()
    train_subsets = dict()
    dev_subsets = dict()
    train_dataloaders = dict()
    dev_dataloaders = dict()
    embedding_models = dict()
    word_voc = []
    languages = LANGUAGES
    if exclude_jap and "japanese" in languages:
        languages.remove("japanese")

    for lang in languages:
        ## Load train data
        print(f"Loading data for {lang.capitalize()}...", end="")
        train_datasets[lang] = Task1Dataset(language=lang, mode="train", word_encoding="char")
        print("Done!", end="\n")
        word_voc += train_datasets[lang].word_voc

        if lang == "japanese":
            japanese_train_analogies, japanese_test_analogies = train_test_split(train_datasets[lang].analogies, test_size=0.3, random_state = JAP_SPLIT_RANDOM_STATE)
            train_datasets[lang].analogies = japanese_train_analogies

        BOS_ID = len(train_datasets[lang].word_voc_id) # (max value + 1) is used for the beginning of sequence value
        EOS_ID = len(train_datasets[lang].word_voc_id) + 1 # (max value + 2) is used for the end of sequence value

        # Get subsets
        if len(train_datasets[lang]) > nb_analogies//len(train_datasets):
            train_indices = list(range(len(train_datasets[lang])))
            train_sub_indices = rd.sample(train_indices, nb_analogies//len(train_datasets))
            train_subsets[lang] = Subset(train_datasets[lang], train_sub_indices)
        else:
            train_subsets[lang] = train_datasets[lang]

        if len(train_datasets[lang]) > 100:
            dev_indices = list(range(len(train_datasets[lang])))
            dev_sub_indices = rd.sample(dev_indices, 100)
            dev_subsets[lang] = Subset(train_datasets[lang], dev_sub_indices)
        else:
            dev_subsets[lang] = train_datasets[lang]

        # Load data
        train_dataloaders[lang] = DataLoader(train_subsets[lang], shuffle = True, collate_fn = partial(collate, bos_id = BOS_ID, eos_id = EOS_ID), batch_size=16, num_workers=16, pin_memory=True)
        dev_dataloaders[lang] = DataLoader(dev_subsets[lang], collate_fn = partial(collate, bos_id = BOS_ID, eos_id = EOS_ID), batch_size=16, num_workers=16, pin_memory=True)
        
        if lang == "japanese":
            char_emb_size=512
        else:
            char_emb_size=64
        embedding_models[lang] = CNNEmbedding(emb_size=char_emb_size, voc_size = len(train_datasets[lang].word_voc_id) + 2) # emb_size is the size of the intermediate character embedding
        if data_parallel:
            embedding_models[lang] = nn.DataParallel(embedding_models[lang], device_ids=list(range(torch.cuda.device_count())))
        embedding_models[lang].to(device)

    def rotation_dataloader(dataloaders):
        # convert dataloaders to the corresponding iterators
        iterators = {lang: iter(dataloaders[lang]) for lang in dataloaders.keys()}
        empty = set()
        # continue grabbing data while some dataloder (in that case its iterator) still has something in it
        while len(empty) < len(iterators): 

            # take a batch from each language in turn, with the order of the languages not defined
            langs = list(dataloaders.keys())
            rd.shuffle(langs)
            for lang in langs:
                if lang not in empty:
                    try:
                        yield next(iterators[lang]), lang
                    except StopIteration:
                        empty.add(lang)


    # --- Training models ---


    classification_model = AnalogyClassification(emb_size=16*5) # 16 because 16 filters of each size, 5 because 5 sizes

    # Data paralelism must be done before putting device on CUDA
    if data_parallel:
        classification_model = nn.DataParallel(classification_model, device_ids=list(range(torch.cuda.device_count())))

    # --- Training Loop ---
    classification_model.to(device)
    parameters = list(classification_model.parameters())
    for embedding_model in embedding_models.values():
        parameters += list(embedding_model.parameters())
    optimizer = torch.optim.Adam(parameters)
    criterion = nn.BCELoss()

    losses_list = []
    dev_losses_list = []
    dev_acc_list = []
    times_list = []

    for epoch in range(epochs):

        losses = []
        with elapsed_timer() as elapsed:
            for (a, b, c, d), lang in rotation_dataloader(train_dataloaders):

                optimizer.zero_grad()

                # compute the embeddings
                a = embedding_models[lang](a.to(device))
                b = embedding_models[lang](b.to(device))
                c = embedding_models[lang](c.to(device))
                d = embedding_models[lang](d.to(device))

                a = torch.unsqueeze(a, 1)
                b = torch.unsqueeze(b, 1)
                c = torch.unsqueeze(c, 1)
                d = torch.unsqueeze(d, 1)

                # to be able to add other losses, which are tensors, we initialize the loss as a 0 tensor
                loss = torch.tensor(0).to(device).float()

                for a_, b_, c_, d_ in enrich(a, b, c, d):
                    # positive example, target is 1
                    is_analogy = classification_model(a_, b_, c_, d_)

                    expected = torch.ones(is_analogy.size(), device=is_analogy.device)

                    loss += criterion(is_analogy, expected)
                
                for a_, b_, c_, d_ in random_sample_negative(a, b, c, d):
                    # negative example, target is 0
                    is_analogy = classification_model(a_, b_, c_, d_)

                    expected = torch.zeros(is_analogy.size(), device=is_analogy.device)

                    loss += criterion(is_analogy, expected)

                # once we have all the losses for one set of embeddings, we can backpropagate
                loss.backward()
                optimizer.step()

                losses.append(loss.cpu().item())

        dev_losses = []
        dev_acc = []
        with elapsed_timer() as dev_elapsed:
            with torch.no_grad():
                for (a, b, c, d), lang in rotation_dataloader(dev_dataloaders):
                    # compute the embeddings
                    a = embedding_models[lang](a.to(device))
                    b = embedding_models[lang](b.to(device))
                    c = embedding_models[lang](c.to(device))
                    d = embedding_models[lang](d.to(device))

                    a = torch.unsqueeze(a, 1)
                    b = torch.unsqueeze(b, 1)
                    c = torch.unsqueeze(c, 1)
                    d = torch.unsqueeze(d, 1)

                    # to be able to add other losses, which are tensors, we initialize the loss as a 0 tensor
                    loss = torch.tensor(0).to(device).float()
                    acc = 0

                    for a, b, c, d in enrich(a, b, c, d):

                        # positive example, target is 1
                        is_analogy = classification_model(a, b, c, d)

                        expected = torch.ones(is_analogy.size(), device=is_analogy.device)

                        loss += criterion(is_analogy, expected)
                        acc += (is_analogy >= 0.5).sum().item()
                        

                        for a, b, c, d in generate_negative(a, b, c, d):

                            # negative examples, target is 0
                            is_analogy = classification_model(a, b, c, d)

                            expected = torch.zeros(is_analogy.size(), device=is_analogy.device)

                            loss += criterion(is_analogy, expected)
                            acc += (is_analogy < 0.5).sum().item()
                    dev_losses.append(loss.cpu().item())
                    dev_acc.append(acc/(32*a.size(0)))

        losses_list.append(mean(losses))
        dev_losses_list.append(mean(dev_losses))
        dev_acc_list.append(mean(dev_acc))
        times_list.append(elapsed())
        print(f"Epoch: {epoch}, Run time: {times_list[-1]:4.5}s, Loss: {losses_list[-1]:>2.8f}, Dev run time: {dev_elapsed():4.5}s, Dev loss: {dev_losses_list[-1]:>2.8f}, Dev accuracy: {dev_acc_list[-1]:>3.4%}")

    torch.save({
        "state_dict_classification": classification_model.cpu().state_dict(),
        "losses": losses_list,
        "times": times_list,
        "langs": {
            lang: {
                "state_dict_embeddings": embedding_models[lang].cpu().state_dict(),
                "voc": train_datasets[lang].word_voc,
                "voc_id": train_datasets[lang].word_voc_id
            }
            for lang in languages
        }
        }, CLASSIFIER_MULTILINGUAL_PATH.format(classifier_multilingual_mode(exclude_jap, True)))

if __name__ == '__main__':
    train_classifier()
