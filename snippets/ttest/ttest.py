import pandas as pd
from scipy.stats import ttest_rel

def ttest(df, column1, column2):
    print(f"{column1} with {column2}: {ttest_rel(df[column1], df[column2])}")

@click.command()
@click.option('--path', default='accuracies/valid.csv', help='The path to the csv file.', show_default=True)
def print_ttest(path):
    df = pd.read_csv(f'{path}',
                        sep=';',
                        index_col=0,
                        header=0,
                        engine='python')
    print(df.columns)
    for i in range(len(df.columns)):
        for j in range(i+1, len(df.columns)):
            col1 = df.columns[i]
            col2 = df.columns[j]
            ttest(df, col1, col2)

if __name__ == '__main__':
    print_ttest()
