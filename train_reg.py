#!/usr/bin/env python
# coding: utf-8

import random as rd
from copy import copy
from functools import partial
from statistics import mean, stdev

import click
import torch
import torch.nn as nn
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader, Subset

from analogy_reg import AnalogyRegression
from cnn_embeddings import CNNEmbedding
from config import (CLASSIFIER_PATH, REGRESSION_PATH, REGRESSION_VECTORS_PATH,
                    duplicate_label)
from data import LANGUAGES, Task1Dataset, enrich
from eval_reg import eval_reg_
from utils import collate, elapsed_timer, generate_embeddings_file_

device = "cuda" if torch.cuda.is_available() else "cpu"

@click.command()
@click.option('--language', '-l', default="german", help='The language to train the model on.', show_default=True, type=click.Choice(LANGUAGES, case_sensitive=False))
@click.option('--nb-analogies', '-n', default=50000,
              help='The maximum number of analogies (before augmentation) we train the model on. If the number is greater than the number of analogies in the dataset, then all the analogies will be used.', show_default=True)
@click.option('--epochs','-e', default=20,
              help='The number of epochs we train the model for.', show_default=True)
@click.option('--epochs-freeze','-f', default=10,
              help='The number of epochs for which the embedding remains frozen.', show_default=True)
@click.option('--test','-t', default=False, is_flag=True,
              help='Wether to directly evaluate the model or not.', show_default=True)
@click.option('--repetitions','-r', default=10,
              help='The number of repetitions of the evaluation, if --test or -t is used.', show_default=True)
@click.option('--duplicate-id','-d', default=0,
              help='The id to use to identify the model in case of an intended duplicate with the same language and number of epochs.', show_default=True)
def train_reg(language, nb_analogies, epochs, epochs_freeze, test, repetitions, duplicate_id):
    print("\n\t".join(["Training parameters:",
        f"language: {language}",
        f"number of analogies: {nb_analogies}",
        f"number of epochs: {epochs} of which {epochs_freeze} with the embedding frozen",
        f"automatic testing at the end: {test} ({repetitions} repetitions per setting)"]))
    
    with elapsed_timer() as total_elapsed:
        path = REGRESSION_PATH.format(language=language, epochs=epochs, duplicate_label=duplicate_label(duplicate_id))
        emb_path = CLASSIFIER_PATH.format(language=language, epochs=20)
        ## Load Embedding model

        emb_data = torch.load(emb_path)

        ## Train and test dataset
        train_dataset = Task1Dataset(language=language, mode="train", word_encoding="char")

        if language == "japanese":
            japanese_train_analogies, japanese_test_analogies = train_test_split(train_dataset.analogies, test_size=0.3, random_state = 42)
            test_dataset = copy(train_dataset)
            test_dataset.analogies = japanese_test_analogies
            train_dataset.analogies = japanese_train_analogies
        else:
            test_dataset = Task1Dataset(language=language, mode="test", word_encoding="char")
        
        # Same dict
        test_dataset.word_voc = train_dataset.word_voc
        test_dataset.word_voc_id = train_dataset.word_voc_id
        BOS_ID = len(train_dataset.word_voc) # (max value + 1) is used for the beginning of sequence value
        EOS_ID = len(train_dataset.word_voc) + 1 # (max value + 2) is used for the end of sequence value
        # Get subsets
        if len(train_dataset) > nb_analogies:
            train_indices = list(range(len(train_dataset)))
            train_sub_indices = rd.sample(train_indices, nb_analogies)
            train_subset = Subset(train_dataset, train_sub_indices)
        else:
            train_subset = train_dataset

        # Load data
        train_dataloader = DataLoader(train_subset, collate_fn = partial(collate, bos_id = BOS_ID, eos_id = EOS_ID), num_workers=8)
        
        ######## Train models ########
        if language == "japanese":
            emb_size = 512
        else:
            emb_size = 64

        regression_model = AnalogyRegression(emb_size=16*5) # 16 because 16 filters of each size, 5 because 5 sizes

        embedding_model = CNNEmbedding(emb_size=emb_size, voc_size = EOS_ID + 1) # len(train_dataset.voc)
        embedding_model.load_state_dict(emb_data['state_dict_embeddings'])
        embedding_model.eval()

        # --- Training Loop ---
        embedding_model.to(device)
        regression_model.to(device)

        optimizer = torch.optim.Adam((list(regression_model.parameters()) + list(embedding_model.parameters())), lr=1e-4)

        criterion = nn.MSELoss()

        losses_list = []
        times_list = []

        for epoch in range(epochs):
            losses = []
            if epoch<epochs_freeze:
                #freeze the embedding model
                embedding_model.train(False)
            else:
                #unfreeze the model
                embedding_model.train(True)

            with elapsed_timer() as elapsed:
                for a, b, c, d in train_dataloader:

                    optimizer.zero_grad()
                    # compute the embeddings
                    a = embedding_model(a.to(device))
                    b = embedding_model(b.to(device))
                    c = embedding_model(c.to(device))
                    d = embedding_model(d.to(device))
                    #print(a.device, b.device, c.device, d.device)

                    # to be able to add other losses, which are tensors, we initialize the loss as a 0 tensor
                    loss = torch.tensor(0).to(device).float()

                    for a_, b_, c_, d_ in enrich(a, b, c, d):
                        d_pred = regression_model(a_, b_, c_, p=0)
                        loss += criterion(d_pred, d_)

                    loss /= 1 + (
                        criterion(a,b) +
                        criterion(a,c) +
                        criterion(a,d) +
                        criterion(b,c) +
                        criterion(b,d) +
                        criterion(c,d))
                    
                    loss.backward()
                    optimizer.step()

                    losses.append(loss.cpu().item())

            losses_list.append(mean(losses))
            times_list.append(elapsed())
            print(f"Epoch: {epoch}, Run time: {times_list[-1]:4.5}s, Loss: {losses_list[-1]}")
        
        model_data = {
            "state_dict": regression_model.cpu().state_dict(),
            "state_dict_embeddings": embedding_model.cpu().state_dict(),
            "losses": losses_list,
            "times": times_list,
            "word_voc": train_dataset.word_voc,
            "word_voc_id": train_dataset.word_voc_id
            }
        torch.save(model_data, path)

    total = total_elapsed()
    print(f"Total training time: {total:4.5}s = {total/60:4.5}min = {total/3600:4.5}h")
    

    print("--- Preparing evaluation embeddings ---")
    with elapsed_timer() as elapsed:
        generate_embeddings_file_(
            train_dataset,
            test_dataset,
            embedding_model,
            language,
            out_path=REGRESSION_VECTORS_PATH.format(language=language, epochs=epochs, duplicate_label=duplicate_label(duplicate_id)))
    print(f"Done in {elapsed():4.5}s")

    if test:
        print("--- Starting test ---")
        eval_reg_(language, nb_analogies, epochs, repetitions, duplicate_id)

if __name__ == '__main__':
    train_reg()
