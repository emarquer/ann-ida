
import baseline.murena.analogy as murena
import baseline.alea.alea as alea
import baseline.lepage.nlg.Analogy.tests.nlg_benchmark as lepage
import data
import pandas as pd
from multiprocessing import Pool
from torch.utils.data import random_split, Subset
import sys


"""
source ~/miniconda3/bin/activate nn-morpho-analogy ; cd ~/orpailleur/emarquer/nn-morpho-analogy ; python run_baseline.py arabic murena
"""

def lepage_wrapper(a,b,c,d):
    result = lepage.is_analogy(f"{a} : {b} :: {c} : {d}")
    if result:
        return True, 0
    else:
        return False, 99999999


MODELS = {
    "murena": murena,
    "alea": alea,
    "lepage": lepage_wrapper}


DATA_AMOUNT = 50000
CHECKPOINT_SIZE = 100
Ns = [1]#[1, 2, 5, 10]
N_MAX = max(Ns)
LANGUAGE = sys.argv[1] if len(sys.argv) > 1 else data.LANGUAGES[0]
MODEL = sys.argv[2] if len(sys.argv) > 2 else "murena"
if MODEL in {"kolmogorov", "kolmo"}: MODEL = "murena"
if LANGUAGE == "jap": LANGUAGE = "japanese"
STORE_RESULTS = True and MODEL != "lepage"  # lepage does not generate D, so we cannot store it
#LANGUAGE = data.LANGUAGES[0]
assert MODEL in MODELS.keys()
assert LANGUAGE in data.LANGUAGES


if LANGUAGE == "japanese":
    dataset_full = data.Task1Dataset(LANGUAGE, "train")
    dataset, _ = random_split(dataset_full, [7618, len(dataset_full)-7618])
else:
    dataset_full = data.Task1Dataset(LANGUAGE, "test")
    if DATA_AMOUNT < len(dataset_full):
        dataset, _ = random_split(dataset_full, [DATA_AMOUNT, len(dataset_full)-DATA_AMOUNT])
    else:
        dataset = dataset_full

full_records = []
for i in range(0, len(dataset), CHECKPOINT_SIZE):
    base = Subset(dataset, [i for i in range(i,min(i+CHECKPOINT_SIZE, len(dataset)))])
    positive = [(a,b,c,d) for a,b,c,d in data.enrich(base)]
    negative = [(a,b,c,d) for a,b,c,d in data.generate_negative(positive)]

    
    def process(x):
        a,b,c,d = x

        if MODEL == "lepage":
            return MODELS[MODEL](a,b,c,d)[1]
        else:
            if STORE_RESULTS:
                return MODELS[MODEL].classification_with_results(a,b,c,d, N_MAX)[1:]
            else:
                return MODELS[MODEL].classification_(a,b,c,d, N_MAX)[1]
    p = Pool()
    print("Starting base examples...")
    base_results = p.map(process, base)
    print("Done!")
    print("Starting positive examples...")
    positive_results = p.map(process, positive)
    print("Done!")
    print("Starting negative examples...")
    negative_results = p.map(process, negative)
    print("Done!")



    
    if STORE_RESULTS:
        full_records.extend([
            {"lang": LANGUAGE, "rank": rank, "input": a, "result": result, "type": "base"} for a, (rank, result) in zip(base, base_results)
        ] + [
            {"lang": LANGUAGE, "rank": rank, "input": a, "result": result, "type": "positive"} for a, (rank, result) in zip(positive, positive_results)
        ] + [
            {"lang": LANGUAGE, "rank": rank, "input": a, "result": result, "type": "negative"} for a, (rank, result) in zip(negative, negative_results)
        ])
    else:
        full_records.extend([
            {"lang": LANGUAGE, "rank": rank, "type": "base"} for rank in base_results
        ] + [
            {"lang": LANGUAGE, "rank": rank, "type": "positive"} for rank in positive_results
        ] + [
            {"lang": LANGUAGE, "rank": rank, "type": "negative"} for rank in negative_results
        ])
    df = pd.DataFrame.from_records(full_records)

    df.to_csv(f"symbolic_baseline/{MODEL}/{LANGUAGE}-max{N_MAX}-{i+DATA_AMOUNT}.csv")

    
    for n in Ns:
        df[f"accuracy@{n}"] = df["rank"] < n
        df.loc[df["type"]=="negative",f"accuracy@{n}"] = df.loc[df["type"]=="negative","rank"] >= n
    print(LANGUAGE, MODEL, f"{i} to {i+CHECKPOINT_SIZE} of {DATA_AMOUNT}")
    #print(df.groupby("type")[[f"accuracy@{n}" for n in Ns]].mean().loc[["base", "positive", "negative"]].transpose().to_string(formatters=[lambda x: f"{x:.2%}"]*len(Ns)*3))
    print(df.groupby("type")[[f"accuracy@{n}" for n in Ns]].mean().loc[["base", "positive", "negative"]].transpose().to_string(formatters=[lambda x: f"&{x*100:.2f}"]*len(Ns)*3))



